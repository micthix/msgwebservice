<?php


class Identify{
	
	private $id;
	private $token;
	private $isLogged;
	
	private static $ic = null;
	
	public static function getInstance(){
		if(Identify::$ic == null){
			Identify::$ic = new Identify();
		}
		return Identify::$ic ;
	}
	
	private function __construct(){
		
	}
	
	
	public function identify(JSonResponse &$response, $appid, $token) {
		if (! isset ( $appid ) || ! isset ( $token ) || strlen ( $appid ) == 0 || strlen ( $token ) == 0) {
			throw new RequestNotValidException();
		} else {
			
			try{
				$hashToken = DAO::getDAO ()->getAppHashToken ($response, $appid);
				
				$givenHash = crypt($token, $hashToken);
				
				if($givenHash != $hashToken){
					throw new AutenticationException();
				}
				
				$this->setId($appid);
				$this->setToken($givenHash);
				$this->setLogged(true);
				
				
				
			}catch (AutenticationException $ae){
				$response->addError($ae);
			}
			
			
		
		}
	}
	
	
	### Setters && getters ###
	
	public function  setLogged($logged){
		$this->isLogged = $logged;
	}
	
	public function isLogged(){
		return $this->isLogged;
	}
	
	public function setToken($token){
		$this->token=$token;
	}
	
	public function getToken(){
		return $this->token;
	}
	
	public function setId($id){
		$this->id = $id;
	}
	
	public function getId(){
		return $this->id;
	}
	
	
	
	
	
}


?>