# README #

AUTHOR
Micha Meier

ABOUT
It's a Web Service that is using JSON for communication. 
The goal is to get saved messages stored in the database by:
* Message ID
* Language code (like en) 
* Application ID 
* Module ID

Further informations about the communication protocol are found in Google Docs:
https://docs.google.com/document/d/1Pd0M0pEojJnVeThHSoOT78UeW3TizZLIEwM9PhlRnvM/edit?usp=sharing

EXECUTION
This project is written in PHP and can be executed v.5.6+.  

DEPENDENCIES
You can turn on URL rewrite, but it's not mandatory.

CONTACT FOR QUESTIONS OR FEEDBACK
micha.meier.siueg@gmail.com