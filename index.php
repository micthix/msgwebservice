<?php



include_once 'view/ViewController.php';

include_once 'controller/MessgeController.php';

include_once 'db/DAO.php';
include_once 'db/DB.php';

include_once 'error/MyException.php';
include_once 'error/AppNotFoundException.php';
include_once 'error/AutenticationException.php';
include_once 'error/DatabaseException.php';
include_once 'error/MessageNotFoundException.php';
include_once 'error/RequestNotFoundException.php';
include_once 'error/RequestNotValidException.php';

include_once 'jsonRequest/JsonRequest.php';
include_once 'jsonRequest/JsonRequestItem.php';

include_once 'jsonResponse/JsonResponse.php';
include_once 'jsonResponse/JsonResponseItem.php';

include_once 'security/identify.php';

include_once 'utils/ClassChanger.php';


header('Content-Type: application/json','charset=utf-8');


$view = new ViewController();	
$view->process();
	
	


	
	
	
	
	
?>