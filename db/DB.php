<?php
class Db {
	private static $host = "localhost";
	private static $port = "3306";
	private static $db = "msg_ws";
	private static $user = "msg_ws";
	private static $password = "tap!1_09WeP?lQwR:";
	private static $singletonDB = null;
	
	private $conn;
	
	
	public function __construct() {

	}
	
	public function getCon() {
		
		if(!isset($this->conn) || $this->conn == null){
			try {
					
				
				$this->conn = new PDO ( "mysql:host=".Db::$host.";dbname=".Db::$db, Db::$user, Db::$password ,
                    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
			
				// set the PDO error mode to exception
				$this->conn->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			} catch ( PDOException $e ) {
				error_log("DB Connection failed: " . $e->getMessage ());
				throw new DatabaseException($e->getMessage());
			}
				
		}		
		
		return $this->conn;
	}

    /**
     * @param JSonResponse $response
     * @param $query
     * @return PDOStatement
     */
	public function myQuery(JSonResponse &$response, $query){
		try {
			return $this->getCon()->query($query);
		} catch (DatabaseException $de) {
			$response->addError($de);
		}
	}
}

?>