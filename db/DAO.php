<?php

class DAO{
	
	private static  $dao = null;
	private $db;
	
	public static function getDAO(){
		if(DAO::$dao == null){
			DAO::$dao = new DAO();
		}
		return DAO::$dao;
	}


    private function __construct(){
		$this->db = new Db();		
	}

    public function  getMessages(JsonRequest $request, JSonResponse &$response){
        $responseItems = array();

        $query_items = $this->prepareRequestItems($request);


        $sql = "SELECT idmsg, type, message FROM messages_view WHERE appid =". $this->db->getCon()->quote($request->getAppId())." AND modid=".$this->db->getCon()->quote($request->getModulId())." AND lang =".$this->db->getCon()->quote($request->getAppLang()) . $query_items;

        $result =$this->db->myQuery($response, $sql);
		
		while($rowObject = $result->fetchObject('JsonResponseItem')){
			$responseItems[] = $rowObject;
		}

		//Not all items could be found -> check one by one
        $this->checkMessageIds($request, $response, $result);

        $response->setResponseItems($responseItems);
	}


	public function getMessage(JsonRequest $request, JSonResponse &$response, $messageId){
        $sql = "SELECT idmsg, type, message FROM messages_view WHERE appid =". $this->db->getCon()->quote($request->getAppId())." AND modid=".$this->db->getCon()->quote($request->getModulId())." AND lang =".$this->db->getCon()->quote($request->getAppLang()) ." AND idmsg =". $this->db->getCon()->quote($messageId);
        $result =$this->db->myQuery($response, $sql);
        return $result->fetchObject('JsonResponseItem');
    }


    /**
     * Get a specific app token
     * @param JSonResponse $response
     * @param $appid
     * @return mixed
     */
	public function getAppToken(JSonResponse &$response, $appid){
		$responseItems = array();
		
		$sql = ("SELECT token FROM applications WHERE appid =". $this->db->getCon()->quote($appid));
		$db = $this->db->getCon()->myQuery($response, $sql);
		
		$rowObject = $db->fetch(PDO::FETCH_ASSOC);
			
		return $rowObject['token'];
	}
	
	/**
	 * Get all id and tokens of registred apps
	 * @return mixed[]
	 */
	public function getApps(JSonResponse &$response){
		$responseItems = array();
	
		$sql = ("SELECT id, token FROM applications") ;
		$db = $this->db->getCon()->myQuery($response, $sql);
	
		while($rowObject = $db->fetch(PDO::FETCH_ASSOC)){
			$responseItems[] = $rowObject;
		}
	
		return $responseItems;
	}
	
	public function getAppHashToken(JSonResponse &$response, $appid){
		
		
		$sql = ("SELECT id, token FROM applications WHERE id =". $this->db->getCon()->quote($appid) ." ");
		$result =$this->db->myQuery($response, $sql);
		if(!($hash = $result->fetchColumn(1))) {
			throw new AppNotFoundException("Application not found!");
		}
		
		return $hash;
	}

    /**
     * @param JsonRequest $request
     * @return string
     */
    public function prepareRequestItems(JsonRequest $request)
    {
        if ($request->getRequItems() == null || sizeof($request->getRequItems()) == 0) {
            $query_items = " ";
            return $query_items;
        } else {
            $items = $request->getRequItems();
            foreach ($items as $key => $msgId) {
                $items[$key] = $this->db->getCon()->quote($msgId);
            }
            $query_items = " AND idmsg IN(" . (implode(",", $items)) . ")";
            return $query_items;
        }
    }

    /**
     * @param JsonRequest $request
     * @param JSonResponse $response
     * @param $result
     */
    public function checkMessageIds(JsonRequest $request, JSonResponse &$response, $result)
    {
        if (sizeof($request->getRequItems()) != 0 && $result->rowCount() < sizeof($request->getRequItems())) {
            foreach ($request->getRequItems() as $msgId) {
                if ($this->getMessage($request, $response, $msgId) == false) {
                    $mnfe = new MessageNotFoundException("", $msgId);
                    $response->addError($mnfe);

                }
            }
        }
    }


}



?>