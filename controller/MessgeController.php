<?php

class MessageController{
	
	private $requObject;
	
	
	public function __construct(){
		
	}
	
	
	public function execute(JsonRequest $request, $requestType, JSonResponse &$response ){
		$this->setRequObject($request);
		//print_r($this->getRequObject());
		
		try{
			Identify::getInstance()->identify($response, $request->getAppId(), $request->getAppToken());
		}catch(MyException $afe){
			$response->addError($afe);
		}

		if(Identify::getInstance()->isLogged()){
            DAO::getDAO()->getMessages($request, $response);
		}
		
	}
	
	
	
	### Getter & Setters ###
	
	public function setRequObject(JsonRequest $object){
		$this->requObject = $object;
	}
	
	public function getRequObject(){
		return $this->requObject;
	}
	
	
	
}


?>
