<?php


class ViewController {
	public function __construct() {
	
	}
	public function process() {
		$controller = new MessageController ();
		$response = new JSonResponse ();
		
		$reqestType = (String)$_SERVER ['REQUEST_METHOD'];
		
		try {
			if (isset ( $_REQUEST ['request'] ) && strlen( $_REQUEST ['request']) != 0) {
				$json = $_REQUEST ['request'];
				
				$this->checkJsonString($response, $json);
				
				$request = json_decode ( $json );
				$requ = new JsonRequest($request->apptoken, $request->appid, $request->modulid, $request->applang, $request->requitems);
				
				$controller->execute ( $requ, $reqestType, $response );

			} else {
				$me = new RequestNotFoundException ();
			}
		} catch ( MyException $me ) {
	
		} catch ( Exception $e ) {
			
			$me = new MyException($e->getMessage(), 400);

		} catch ( Error $e ) {
			$me = new MyException($e->getMessage(), 400);
			
		} finally {
			if(isset($me)){
				$response->addError ( $me );
			}
		}

		print (json_encode($response));

	}
	
	public function getJson() {
	}
	
	public function printJson() {
	}
	
	public function checkJsonString(JSonResponse $response, $jsonString){
		$reflector = new ReflectionClass('JSonRequest');
		
		
		$properties = $reflector->getDefaultProperties();
		foreach ($properties as $key => $value){
			if(!strpos($jsonString,'"'.$key.'"')){
				$response->addError(new RequestNotValidException("Please check your request, it doesn't contains: ". $key));
			}
		}
		
		
	}
}

?>